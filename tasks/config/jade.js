module.exports = function(grunt) {
  grunt.config.set("jade", {
    dev: {
      expand: true,
      cwd: "src/app",
      src: ["**/*.jade"],
      dest: "templates/",
      ext: ".html",
      options: {
        pretty: true
      }
    }
  });
  grunt.loadNpmTasks("grunt-contrib-jade");
};

