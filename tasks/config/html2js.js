module.exports = function(grunt) {

  grunt.config.set('html2js', {
    options: {
      htmlmin: {
        collapseBooleanAttributes: true,
        collapseWhitespace: true,
        removeAttributeQuotes: true,
        removeComments: true,
        removeEmptyAttributes: true,
        removeRedundantAttributes: true,
        removeScriptTypeAttributes: true,
        removeStyleLinkTypeAttributes: true
      }
    },
    main: {
      src: ['templates/**/*.html'],
      dest: 'src/app/templates.js'
    }
  });

  grunt.loadNpmTasks('grunt-common-html2js');
};