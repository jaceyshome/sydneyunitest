module.exports = function(grunt) {
  grunt.config.set('watch', {
    assets: {
      // Assets to watch:
      files: ['src/**/*.jade', '**/*.less'],
      // When assets are changed:
      tasks: ['build']
    }
  });
  grunt.loadNpmTasks('grunt-contrib-watch');
};
