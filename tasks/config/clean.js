module.exports = function(grunt) {

  grunt.config.set('clean', {
    dev: [".tmp/*", "templates/*"]
  });

  grunt.loadNpmTasks('grunt-contrib-clean');
};
