module.exports = function (grunt) {
  grunt.registerTask('build', [
    'clean:dev',
    'less:dev',
    'jade:dev',
    'html2js'
  ]);
};
