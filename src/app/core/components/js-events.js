(function(){
  //------------------- reference: Angularjs js events handler -------------------------------------
  ('click dblclick mousedown mouseup mouseover mouseout mousemove mouseenter mouseleave keydown keyup keypress submit focus blur copy cut paste'.split(' ')).forEach(function(eventName){
    var marker = 'js',
      componentName = marker+eventName.charAt(0).toUpperCase() + eventName.slice(1),
      attrName = marker+'-'+eventName;
  //----------------------------------- end reference -------------------------------------
    lark.addComponent(componentName,[function(){
      return function(){
        return {
          link: (function($scope,$element){
            var exp = $element.getAttribute(attrName) || $element.getAttribute('data-'+attrName);
            $element.addEventListener(eventName, function(event){
              //TODO a simple way to pass $event to $scope, it is not a good solution
              $scope.$event = event;
              $scope.$getExpValue(exp);
              $scope.$update();
            }, false);
          })
        }
      }
    }]);
  });
})();

