

lark.addService('$Class',[function(){
  var service = {};

  //reference: http://stackoverflow.com/questions/14104881/add-remove-class-to-a-dom-element-using-only-javascript-and-best-of-these-2-way

  service.hasClassName = function(element,name){
    return new RegExp("(?:^|\\s+)" + name + "(?:\\s+|$)").test(element.className);
  };

  service.addClassName = function(element,name){
    if (!service.hasClassName(element, name)) {
      element.className = element.className ? [element.className, name].join(' ') : name;
    }
  };

  service.removeClassName = function(element,name){
    if (service.hasClassName(element,name)) {
      var c = element.className;
      element.className = c.replace(new RegExp("(?:^|\\s+)" + name + "(?:\\s+|$)", "g"), "");
    }
  };

  // ------------------------------- end reference -------------------------------------------
  return service;
}]);

lark.addComponent('jsClass',['$Class',function($Class){
  return function(){
    return {
      link: (function($scope,$element){

        //It is better to use function as expression,
        //as too many params on expression will make page hard to read
        var expression = $element.getAttribute('js-class') || $element.getAttribute('data-js-class');
        var oldVal = null;
        $scope.$watch(expression,function(val){
          if(val && oldVal != val){
            $Class.removeClassName($element,oldVal);
            $Class.addClassName($element,val);
            oldVal = val;
          }else{
            if(oldVal == null){
              return;
            }
            $Class.removeClassName($element,oldVal);
            oldVal = null;
          }
        });
      })
    }
  }
}]);


