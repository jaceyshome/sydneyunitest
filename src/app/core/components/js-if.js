lark.addComponent('jsIf',[function(){
  return function(){
    return {
      link: (function($scope,$element){
        var expression = $element.getAttribute('js-if') || $element.getAttribute('data-js-if');
        var $parentNode = $element.parentNode;
        $element.style.display = 'none';
        $scope.$watch(expression,function(val){
          if(val && !$element.parentNode != $parentNode){
            $element.style.display = 'none';
            $parentNode.appendChild($element);
            $($element).fadeIn();
          }else{
            $($element).fadeOut(400, function(){
              $parentNode.removeChild($element);
            });
          }
        });
      })
    }
  }
}]);

