lark.addComponent('jsShow',[function(){
  return function(){
    return {
      link: (function($scope,$element){
        var expression = $element.getAttribute('js-show') || $element.getAttribute('data-js-show');
        $scope.$watch(expression,function(val){
          $element.style.display = 'none';
          if(val){
            //$element.style.display = 'block';
            $($element).fadeIn();
          }else{
            //$element.style.display = 'none';
            $($element).fadeOut(400);
          }
        });
      })
    }
  }
}]);

