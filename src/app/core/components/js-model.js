lark.addComponent('jsModel',[function(){
  return function(){
    return {
      link: (function($scope,$element){
        var expression = $element.getAttribute('js-model') || $element.getAttribute('data-js-model');
        var oldValue;
        if(expression){
          $element.value = $scope.$getExpValue(expression);
        }
        $scope.$watch(expression,function(val){
          $element.value = (val === undefined)? '' : val;
          oldValue = val;
        });

        $element.addEventListener("keyup",function(e){
          oldValue = e.target.value;
          $scope.$setExpValue(expression, oldValue);
          $scope.$update();
          (e.target).focus(); //Fixed Firefox and IE, after update, lost focus on element
        });
      })
    }
  }
}]);

