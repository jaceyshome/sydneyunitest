lark.addService('HTTP',["Config", function(Config){
  //API url is saved in Config service

  var api = Config.api;
  var defaultErrorMessage = "server error, please try again or contact administrator.";
  var dataError = "action failed, please try again.";

  var service = {};
  service.queryOptions = [];

  //------------------ api -------------------
  service.list = function(query, cb){
    $.ajax({
      type: "GET",
      dataType: 'jsonp',
      url: api.concat("records/search.json?q=").concat(query)
    }).done(function(res) {
      if(res.ok == false){
        alert(dataError);
      }
      if(typeof cb == "function"){
        cb(res.data.results);
      }
      lark.$refresh.loop();
    }).error(function(err){
      alert(defaultErrorMessage);
      lark.$refresh.loop();
    });
  };

  service.specify = function(query, cb){
    $.ajax({
      type: "GET",
      dataType: 'jsonp',
      url: api.concat("records/get.json?id=").concat(query)
    }).done(function(res) {
      if(res.ok == false){
        alert(dataError);
      }
      if(typeof cb == "function"){
        cb(res.data);
      }
      lark.$refresh.loop();
    }).error(function(err){
      alert(defaultErrorMessage);
      lark.$refresh.loop();
    });
  };

  return service;
}]);