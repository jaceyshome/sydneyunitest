lark.addComponent('sydneyUniPhoneBook',['HTTP','Helper',function(HTTP, Helper){
  return function(){
    return {
      scope: {},
      templateUrl: "templates/components/phonebook/phonebook.html",
      link: (function($scope,$element){
        var cachedQuery = null;
        var $cardList = null;
        var loading = false;
        var selectedCard = null;
        var isPageEnd = false;  //If all cards have been download, disable sending request to server

        $scope.viewingCardSummary = null;
        $scope.query = '';
        $scope.results = [];

        //-------------------- init ---------------------------
        function init(){
          $cardList = $($element).find(".card-list-container")[0];
          if(Helper.isDevice().android()){
            $cardList.ontouchmove = checkCardListScrollPosition;
          }else{
            $cardList.onscroll = checkCardListScrollPosition;
          }
          registerLoadingCardSummaryEvents();
        }

        function registerLoadingCardSummaryEvents(){
          $scope.$on("$onLoadingCardSummary", function(){
            loading = true;
          });
          $scope.$on("$UpdateCardSummary", function(){
            loading = false;
          })
        }

        function checkCardListScrollPosition(e){
          var offset = 38;
          if(loading == true || isPageEnd == true){
            return;
          }
          if(
              $cardList.offsetHeight + $cardList.scrollTop >=
              $cardList.scrollHeight - offset
            ){
            search();
          }
        }

        //-------------------- handler query  -----------------
        function validateSearch(){
          return !(
            $scope.query == undefined ||
            $scope.query.length === 0 ||
            $scope.query == cachedQuery
          );
        }

        function search(){
          if(isPageEnd == true){
            return;
          }
          var query = $scope.query+"&start="+($scope.results.length+1);
          loading = true;
          HTTP.list(query, handleSearchResult);
          $scope.$update();
        }

        function handleSearchResult(_results, err){
          if(_results.length === 0 && $scope.results.length > 0){
            isPageEnd = true;
          }else{
            $scope.results = $scope.results.concat(_results);
          }
          if($scope.results.length === 0){
            alert("No result found.");
          }
          loading = false;
          cachedQuery = $scope.query;
          $scope.$update();
        }

        function reset(){
          $scope.viewingCardSummary = null;
          $scope.$emit("$ResetCardSummary");
          selectedCard = null;
          $scope.results = [];
          isPageEnd = false;
        }

        //-------------------- public functions ------------------
        $scope.onClickBtnSearch = function(){
          if(validateSearch()){
            reset();
            search();
          }
        };

        $scope.onInputKeydown = function($e){
          if($e.keyCode == 13 && validateSearch()){ //Enter key code
            reset();
            search();
          }
        };

        $scope.setCardListClass = function(){
          if($scope.viewingCardSummary != null){
            return "showingCardSummary";
          }else{
            return "";
          }
        };

        $scope.setLoadingPingClass = function(){
          return (loading == true)? "show" : "hide";
        };

        //---------------- functions called by child phone card ---------
        $scope.setCurrentCard = function(card){
          selectedCard = card;
        };

        $scope.checkCurrentCard = function(card){
          return (card == selectedCard);
        };

        //------------------------- init() ---------------------
        init();

      })
    }
  }
}]);

