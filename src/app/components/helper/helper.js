lark.addService('Helper',[function(){
  var service = {};
  //Reference: http://stackoverflow.com/questions/12606245/detect-if-browser-is-running-on-an-android-or-ios-device
  service.isDevice = function() {
    return {
      android: function() {
        return /Android/i.test(navigator.userAgent);
      },
      blackberry: function() {
        return /BlackBerry/i.test(navigator.userAgent);
      },
      ios: function() {
        return /iPhone|iPad|iPod/i.test(navigator.userAgent);
      },
      windows: function() {
        return /IEMobile/i.test(navigator.userAgent);
      },
      any: function() {
        return this.android() || this.blackberry() || this.ios() || this.windows();
      }
    };
  };

  return service;
}]);