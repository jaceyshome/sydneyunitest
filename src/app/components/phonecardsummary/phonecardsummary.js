lark.addComponent('phoneCardSummary',[function(){
  return function(){
    return {
      scope: {},
      templateUrl: "templates/components/phonecardsummary/phonecardsummary.html",
      link: (function($scope,$element){
        $scope.card = null;

        $scope.$on("$UpdateCardSummary", function(data){
          $scope.card = data;
          $scope.$$parent.viewingCardSummary = data;
        });

        $scope.$on("$ResetCardSummary", function(){
          $scope.card = null;
        });

        $scope.close = function(){
          $scope.card = null;
          $scope.$$parent.viewingCardSummary = null;
          $scope.$$parent.setCurrentCard(null);
          $scope.$update();
        };
      })

    }
  }
}]);

