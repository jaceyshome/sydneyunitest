lark.addComponent('phoneCard',['HTTP',function(HTTP){
  return function(){
    return {
      scope: {
        item: "="
      },
      templateUrl: "templates/components/phonecard/phonecard.html",
      link: (function($scope,$element){
        var firstLetter = null;

        function init(){
          if($scope.item == undefined){
            return;
          }
          if($scope.item.obj.type != "organisation"){
            firstLetter = ($scope.item.obj.name.charAt(0)).toUpperCase();
          }
        }

        //-------------------- public functions ------------------
        $scope.select = function(){
          $scope.$$parent.setCurrentCard($scope.item);
          $scope.$broadcast("$onLoadingCardSummary");
          HTTP.specify($scope.item.obj.id, handleQueryResult);
        };

        $scope.setCurrentCardClass = function(){
          return ($scope.$$parent.checkCurrentCard($scope.item))? "current" : "";
        };

        $scope.setPhoneCardIconClass = function(){
          if(firstLetter == null){
            return;
          }
          return "sprite-sprite sprite-characters_"+firstLetter;
        };

        //-------------------- handlers --------------------------
        function handleQueryResult(data){
          lark.$rootScope.$broadcast("$UpdateCardSummary", data);
        }

        //-------------------- init() ----------------------------
        init();

      })
    }
  }
}]);

